[返回上一级](..)

[上一课](../16-面向对象)

# numpy

numpy是python中常用的一个模块之一，主要提供一些关于代数和向量的类和函数。

numpy提供了非常好的官方文档，遇到什么问题，或者刚开始学习，都可以参考，本网页，主要参考numpy文档中的quick start制作<https://numpy.org/doc/stable/user/quickstart.html>，挑选其中较常用的函数和功能介绍。

## 使用numpy创建一个数组

```python
#使用numpy模块
import numpy as np
#使用其他类型的数据来构建numpy中的数组
a=np.array([[1,2,3],[4,5,6]])
print(a.ndim,a.shape,a.size,a.dtype,a.itemsize,a.data)
a=np.array([2,3,4])
print(a.ndim,a.shape,a.size,a.dtype,a.itemsize,a.data)
a=np.array([2.3,3.4,4.5])
print(a.ndim,a.shape,a.size,a.dtype,a.itemsize,a.data)
a=np.array([[1.1,2,3],[4,5,6]])
print(a.ndim,a.shape,a.size,a.dtype,a.itemsize,a.data)
a=np.array([[1,2,3],[4,5,6]],dtype=float)
print(a.ndim,a.shape,a.size,a.dtype,a.itemsize,a.data)
a=np.array([(1,2,3),[4,5,6]],dtype=float)
print(a.ndim,a.shape,a.size,a.dtype,a.itemsize,a.data)
#常用的创建一个数组的函数
np.zeros((3,4))
np.ones((3,4))
np.empty((2,3))
np.empty((2,),dtype=np.bool)
np.arange(10,30,5)
np.linspace(0,2,9)
# 还有些不常用的，当大家读到不认识的函数时，百度，看它的文档，慢慢积累，编程的学习就是这样子的。
```

## 基本操作

```python
import numpy as np
#加法
a=np.ones((4,2))
b=np.ones((4,2))
print(a+b)
c=np.linspace(0,2,8).reshape((4,2))
print(a+c)
c=np.linspace(0,2,2)
print(a+c)
c=np.linspace(0,2,4).reshape((4,1))
print(a+c)
c=2
print(a+2)
#减法
print(a-b)
print(a-2)
#乘法
a=np.arange(12).reshape((4,3))
b=a
print(a*b)
print(a*2)
print(a**2)
print(a.dot(b.T))
#除法
print(a/b)
print(a/2)

```

## 访问、截取numpy数组

```python
import numpy as np
#一维numpy数组的方式，访问的方式基本和列表是一样的
a=np.arange(12)
print(a[2])
print(a[2:5])
print(a[1:7:2])
a[1:7:2]=200
print(a)
print(a[::-1])
for i in a:
	print(i**2)
```

```python
import numpy as np
#二维数组及以上
a=np.arange(12).reshape((4,3))
print(a[2,2])
print(a[:4,:-1])
print(a[::2,::-1])
a[::2,::2]=300
print(a)
a[::2,::2]=[[10,20],[30,40]]
print(a)
print(a[-1])
print(a[:,2])
```

```python
#用bool类型来截取数组
import numpy as np
a=np.arange(12).reshape((4,3))
b=a>4
print(a[b])
a[b]=-1
print(a)
```

## 用于计算numpy数组的函数


```python
import numpy as np
a=np.arange(12).reshape((4,3))
#e的多少次方，开方，和，最大值，最小值，标准差，方差，相加
print(np.exp(a))
print(np.sqrt(a))
print(np.sum(a))
print(np.max(a))
print(np.min(a))
print(np.std(a))
print(np.var(a))
print(np.add(a,a))
#numpy仍然还提供了很多函数，大家不需要都知道，遇到了，现去百度就可以了，次数多了，常用的就记住了，一次想要把numpy中所有的函数都学会，这样学编程，不好。
```

## 用于改变数组形状的函数

```python
import numpy as np
a=np.arange(12).reshape((4,3))
a=a.flatten()
a=a.reshape((3,4))
a=a.ravel()
a.resize((6,2))
```

## 用于合并两个数组的函数

```python
import numpy as np
a=np.arange(12).reshape((4,3))
b=np.arange(6).reshape((2,3))
print(np.vstack((a,b)))
b=np.arange(24).reshape((4,6))
print(np.hstack((a,b)))
```

## 复制数组

当我们把一个数组赋值给另一个数组时，有两种方式view(shallow copy)和deep copy。

### 浅复制

当把一个数组赋值给另一个变量时，仅仅是把原数组的一个引用（地址）赋值给了新的变量，而没有为新变量开辟新的数组空间用于存储原数组的数据。

```python
import numpy as np
a=np.arange(12).reshape((4,3))
b=a
b[0,0]=100
c=a.view()
c[0,1]=200
d=a.reshape((3,4))
d[0,3]=300
e=a[:,:-1]
e[1,1]=400
f=a[:]
f[0,2]=500
print(a)
```

### 深复制

赋值时，为新的变量开辟一段数组空间，将原数组的所有内存信息，复制给新的变量。两个数组，完全独立。

```python
import numpy as np
a=np.arange(12).reshape((4,3))
b=a.copy()
a[:]=200
print(a,b)
```

## 课后习题

- 求np.arange(16).reshape((4,4))的逆矩阵。
- 求np.arange(16).reshape((4,4))的特征值和特征向量。

[下一课](../18-matplotlib)























