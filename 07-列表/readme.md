[返回上一级](..)

[上一课](../06-字符串)

# 列表

列表是python中最常用的数据结构之一，是一批数据的有序集合，与C和Java中的数组不同，python列表中，元素之间的类别可以不一致。

```python
list1=['abc','def',123,456]
list2=['ttt',333,list1]
list3=list('abcdef')
print('%s\n%s\n%s'%(list1,list2,list3))
```

## 基本操作

列表中常用的几种操作分别为：增删改查

```python
#定义一个空列表
a=[]
##################增##################
#增加一个元素
a.append(2)
a.append(4)
#扩增列表n倍
a=a*4
#增加一个列表
a=a+['a','b']
print(a)
#################删####################
#删除下标4对应的元素
del a[4]
#删除值为4的元素，只会删掉第一个4
a.remove(4)
print(a)
#################改####################
a[2]='c'
a[3:4]=['d']
a[:2]=[6,7]
print(a)
################查#####################
print(a[3])
print(a[2:6])
if 'a' in a:
	print('a in list %s'%a)
else:
	print('a not in list %s'%a)
for i in a:
	print(i)
for i in range(len(a)):
	print('%d:%s'%(i,a[i]))
```

## 列表函数和方法

|函数|描述|
|:-:|-|
|len(list)|列表元素个数|
|max(list)|返回列表元素最大值|
|min(list)|返回列表元素最小值|
|list(seq)|将元组转换为列表|
|list.append(obj)|在列表末尾添加新的对象|
|list.count(obj)|统计某个元素在列表中出现的次数|
|list.extend(seq)|在列表末尾一次性追加另一个序列中的多个值（用新列表扩展原来的列表）|
|list.index(obj)|从列表中找出某个值第一个匹配项的索引位置|
|list.insert(index, obj)|将对象插入列表|
|list.pop([index=-1])|移除列表中的一个元素（默认最后一个元素），并且返回该元素的值|
|list.remove(obj)|移除列表中某个值的第一个匹配项|
|list.reverse()|反向列表中元素|
|list.sort( key=None, reverse=False)|对原列表进行排序|
|list.clear()|清空列表|
|list.copy()|复制列表|

以上的函数大多数都是比较常用的，也是因为列表是python中常用的一种数据结构，看定义都很直观，但有一个函数需要特别地演示一下：

```python
#复制列表函数的演示
a=[1,2,3,4,5,6]
b=a
c=a.copy()
b.append('b')
c.append('c')
print('a:%s\nb:%s\nc:%s'%(a,b,c))
#当列表作为函数的参数时的演示
def fun_list(x):
	x.append('fun_list')
	return None
def fun_int(x):
	x=x+1
	return None
x_list=[1,2,3,4]
x_int=4
fun_list(x_list)
fun_int(x_int)
print('%s\n%d'%(x_list,x_int))
```

## 课后习题

- 约瑟夫生者死者小游戏：30 个人在一条船上，超载，需要 15 人下船。于是人们排成一队，排队的位置即为他们的编号。报数，从 1 开始，数到 9 的人下船。如此循环，直到船上仅剩 15 人为止，问都有哪些编号的人下船了呢？
- 计算 n 个自然数的立方和
- 计算数组元素之和
- 将列表中的头尾两个元素对调
- 翻转列表，不使用reverse函数
- 判断元素是否在列表中存在
- 计算元素在列表中出现的次数

[下一课](../08-元组)























