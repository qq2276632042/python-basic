[返回上一级](..)

[上一课](../01-简介)

# 环境搭建

## python的安装

python的安装方式可以分为两种：

1. 单纯的python安装：从python的官网<https://www.python.org/>下载安装包进行安装，而在linux下还需要编译安装的方式，适合对系统的认识比较好的同志；
2. Anaconda集成环境安装，安装方式简单，<https://www.anaconda.com/>；

在日常的编程之中，python往往需要调用各种第三方包，第三方包的安装需要通过conda或pip等管理工具来进行，而如果我们从Anaconda官网下载的安装包，进行安装，当安装第三方包的时候，就要面临“国内访问国外网站的速度”，因此在这里我们推荐使用“清华开源软件站(<https://mirrors.tuna.tsinghua.edu.cn/>)提供的Anaconda”来安装python，其默认第三方包下载源地址在国内，速度很快。

安装步骤(Windows)：
1. 下载Anaconda python集成环境<https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/Anaconda3-4.2.0-Windows-x86_64.exe>；
2. 双击运行，一顿next，最后install即可。

### 运行python

运行python脚本，或者使用python的方式有很多：
1. 最基础的，在命令行中输入python；![图片加载错误](../imgs/0201.PNG "运行python")
2. 如果使用了Anaconda安装python，可以使用Spyder、jupyter、jupyter notebook等集成环境运行python(我自己电脑上不是用Anaconda装的，所以无图可截)；
3. 脚本运行，在命令行界面中使用，python hellowordl.py，执行脚本；

![图片加载错误](../imgs/0202.PNG "运行python")

## Pycharm安装

PyCharm是python的一种应用最为广泛的IDE，如同Eclipse是Java的一种IDE一样。PyCharm分为Professional版和Community版，其中的Community版是免费、开源的，而Professional版是功能比较多的收费版。

安装步骤(Windows)：
1. 从官网下载Pycharm Community版的安装包<https://www.jetbrains.com/pycharm/download/#section=windows>；
2. 双击运行，一顿next，最后install即可。

### 运行python

PyCharm运行python脚本的方式参照[04-hello-world](../04-hello-world)

## 课后习题

集成环境简化了安装的步骤，但也使我们更加地远离真实的python，思考、尝试，不使用Anaconda，使用python官网提供的安装程序，安装python，安装pip包管理工具，指定pip源为国内源，下载PyCharm，配置一个尽量精简的python开发环境。

[下一课](../03-基础语法)























