[返回上一级](..)

[上一课](../02-环境搭建)

# 基础语法

## 标识符

标识符的规定：

1. 第一个字符必须是字母表中字母或下划线 _ 。
2. 标识符的其他的部分由字母、数字和下划线组成。
3. 标识符对大小写敏感。

## 保留字

保留字即关键字，我们不能把它们用作任何标识符名称。python 的标准库提供了一个 keyword 模块，可以输出当前版本的所有关键字：

```python
import keyword
print(keyword.kwlist)
```

## 注释

python中单行注释以 # 开头，多行注释可以使用三个单引号或三个双引号，实例如下：

```python
'''
这是一个用三个单引号制作的多行注释。
这个脚本用来输出python中的保留字。
'''
import keyword
#输出keyword.kwlist的内容
print(keyword.kwlist)#使用print函数
"""
这是一个用三个双引号制作的多行注释。
预计输出的内容应该如下：['False', 'None', 'True', 'and', 'as', 'assert', 'break', 'class', 'continue', 'def', 'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda', 'nonlocal', 'not', 'or', 'pass', 'raise', 'return', 'try', 'while', 'with', 'yield']
"""
```

## 行与缩进

python最具特色的就是使用缩进来表示代码块，不需要使用大括号 {} 。

缩进的空格数是可变的，但是同一个代码块的语句必须包含相同的缩进空格数。实例如下：

```python
#这是一个正确的示例，以缩进来代表代码块
b=True
bb=False
if b:
	print("True")
	if bb:
		print("True")
	else:
		print("False")
else:
	print("False")
	if bb:
		print("True")
	else:
		print("False")
```

```python
#这是一个正确的示例，以空格来代表代码块，你可以使用1个或多个，但一般来说，约定俗成，大家都用4个空格。
b=True
bb=False
if b:
    print("True")
    if bb:
        print("True")
    else:
        print("False")
else:
    print("False")
    if bb:
        print("True")
    else:
        print("False")
```

```python
#这是一个较常见的错误的示例，以空格和缩进一起来代表代码块。
b=True
bb=False
if b:
    print("True")
    if bb:
        print("True")
    else:
        print("False")
else:
    print("False")
    if bb:
        print("True")
	else:
		print("False")
```

**注意事项：**目前很多的编辑器，包括PyCharm一般都会自动处理“缩进字符”，直接用4个空格来代替，并且，这个功能是可以通过配置来更改的，当我们在不同的机器、开发环境下编辑同一个代码文件的时候，就可能因为这种处理“缩进字符”的设定不同导致一些问题。

## 多行语句
python 通常是一行写完一条语句，但如果语句很长，我们可以使用反斜杠(\)来实现多行语句，例如：

```python
a=1
b=2
c=3
s = a + \
	b + \
	c
print(s)
ss=[1,2,3
	,
	4,5,6]
print(ss)
```

## 同一行显示多条语句

python可以在同一行中使用多条语句，语句之间使用分号(;)分割，以下是一个简单的实例：

实例(python 3.0+)
```python
#同行多条语句演示
a='hello';b="world";print(a,b);c=a+' '+b;print(c)
```

## 多个语句构成代码组
缩进相同的一组语句构成一个代码块，我们称之代码组。

像if、while、def和class这样的复合语句，首行以关键字开始，以冒号( : )结束，该行之后的一行或多行代码构成代码组。

我们将首行及后面的代码组称为一个子句(clause)。

如下实例：

```python
def fun(a,b):
	c=a+' '+b
	if '\n' in c:
		c=c.replace('\n','')
		c=c.replace('\\','')
	else:
		pass
	return c
print(fun('hello','world'))
print(fun('hello\n','world'))
print(fun('hello\\','\nworld\n'))
```

## import 与 from...import
在 python 用 import 或者 from...import 来导入相应的模块。

将整个模块(somemodule)导入，格式为： import somemodule

从某个模块中导入某个函数,格式为： from somemodule import somefunction

从某个模块中导入多个函数,格式为： from somemodule import firstfunc, secondfunc, thirdfunc

将某个模块中的全部函数导入，格式为： from somemodule import *

导入 sys 模块
```python
#import演示
import sys
for i in sys.argv:
	print(i)
print(sys.path)
```

```python
#from import 演示
from sys import argv,path
for i in argv:
	print(i)
print(path)
```

## 课后习题

逐一执行本小节中的每一行代码，观看其结果，配合度娘，思考其意义，尝试简单地修改这些代码，观看其效果，这样可以学习这些函数，这种学习的积累可以帮助我们慢慢熟练地掌握python这门语言。

[下一课](../04-hello-world)























