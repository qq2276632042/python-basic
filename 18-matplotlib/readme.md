[返回上一级](..)

[上一课](../17-numpy)

# matplotlib

matplotlib是一种在python下画图用的包，它提供了很多画图的函数和策略，最常用的是一种基于matplotlib.pyplot的方式，简洁。

```python
import numpy as np 
from matplotlib import pyplot as plt 
x = np.arange(1,11) 
y =  2  * x +  5 
plt.title("Matplotlib demo") 
plt.xlabel("x axis caption") 
plt.ylabel("y axis caption") 
plt.plot(x,y) 
plt.show()
```

在matplotlib官网上的documentation里，有绘制各种各样的图的例子，大家有想画什么图形的时候，可以有针对性地参考。<https://matplotlib.org/tutorials/index.html>，我挑出常用的几个，介绍一下。

## 散点图

![](../imgs/1801.png)

```python
import numpy as np
import matplotlib.pyplot as plt
N = 1000
x = np.random.randn(N)
y = np.random.randn(N)
plt.scatter(x, y)
plt.show()
```

## 画线

![](../imgs/1802.png)


```python
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t = np.arange(0.0, 2.0, 0.01)
s = 1 + np.sin(2 * np.pi * t)

plt.plot(t,s)
plt.title("Matplotlib demo")
plt.xlabel("x axis caption")
plt.ylabel("y axis caption")

plt.show()
```

## 直方图

![](../imgs/1803.png)

```python
import numpy as np
import matplotlib.pyplot as plt

# Fixing random state for reproducibility
np.random.seed(0)

mu, sigma = 100, 15
x = mu + sigma * np.random.randn(10000)

# the histogram of the data
plt.hist(x, 50, density=True, facecolor='g', alpha=0.75)


plt.xlabel('Smarts')
plt.ylabel('Probability')
plt.title('Histogram of IQ')
plt.xlim(40, 160)
plt.ylim(0, 0.03)
plt.grid(True)
plt.show()
```

## 柱状图

![](../imgs/1804.png)

```python
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t=np.arange(1,6)
#names=list(map(lambda x:'name%d'%x,t))

plt.bar(t,t)
#plt.bar(names,t,label='a')
#plt.bar(names,np.ones(5),bottom=t,label='b')


#plt.imshow(t,cmap='gray')

plt.title("Matplotlib demo")
plt.xlabel("x axis caption")
plt.ylabel("y axis caption")
#plt.legend()

plt.show()
```

## 箱线图

![](../imgs/1805.png)

```python
import numpy as np
import matplotlib.pyplot as plt
data=np.random.randint(1,100,400).reshape((100,4))
plt.figure(figsize=(5,6))
plt.boxplot(data)
plt.show()
```

## 画图像

![](../imgs/1806.png)

```python
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t=np.arange(256).reshape((16,16))

plt.imshow(t)
#plt.imshow(t,cmap='gray')

plt.title("Matplotlib demo")
plt.xlabel("x axis caption")
plt.ylabel("y axis caption")

plt.show()
```

![](../imgs/1807.png)

```python
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
NBA=pd.read_csv(r"http://datasets.flowingdata.com/ppg2008.csv")
fig,ax=plt.subplots(figsize=(6,6))

score=NBA.loc[:,"G":"PF"].values
name=NBA.iloc[:,0]
col=NBA.loc[:,"G":"PF"].columns

im=ax.imshow(score,cmap='plasma_r') #用cmap设置配色方案
ax.xaxis.set_ticks_position('top') #设置x轴刻度到上方
ax.set_xticks(np.arange(len(col))) #设置x轴刻度
ax.set_yticks(np.arange(len(name))) #设置y轴刻度
ax.set_xticklabels(col,fontsize=4) #设置x轴刻度标签
ax.set_yticklabels(name,fontsize=6) #设置y轴刻度标签
fig.colorbar(im,pad=0.03) #设置颜色条
ax.set_title("NBA Average Performance (Top 50 Players)",x=0.2,y=1.034,fontsize=16) #设置标题以及其位置和字体大小

plt.show()
```

![](../imgs/1808.png)

```python
import numpy as np
import matplotlib.pyplot as plt
f=np.load('mnist.npz')
x_train, y_train = f['x_train'], f['y_train']
x_test, y_test = f['x_test'], f['y_test']
plt.imshow(x_train[600],cmap='gray')
plt.show()
```

![](../imgs/1809.png)

```python
import numpy as np
import gzip
import matplotlib.pyplot as plt

paths = [
	'train-labels-idx1-ubyte.gz', 'train-images-idx3-ubyte.gz',
	't10k-labels-idx1-ubyte.gz', 't10k-images-idx3-ubyte.gz'
]
with gzip.open(paths[0], 'rb') as lbpath:
	y_train = np.frombuffer(lbpath.read(), np.uint8, offset=8)

with gzip.open(paths[1], 'rb') as imgpath:
	x_train = np.frombuffer(
	imgpath.read(), np.uint8, offset=16).reshape(len(y_train), 28, 28)

with gzip.open(paths[2], 'rb') as lbpath:
	y_test = np.frombuffer(lbpath.read(), np.uint8, offset=8)

with gzip.open(paths[3], 'rb') as imgpath:
	x_test = np.frombuffer(imgpath.read(), np.uint8, offset=16).reshape(len(y_test), 28, 28)

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

plt.figure(figsize=(10,10))
for i in range(25):
	plt.subplot(5,5,i+1)
	plt.xticks([])
	plt.yticks([])
	plt.grid(False)
	plt.imshow(x_train[i], cmap=plt.cm.binary)
	plt.xlabel(class_names[y_train[i]])
plt.show()
```



## 课后习题

- 深度学习最出彩的领域便是关于对图像的处理，在本次集训中，大家用到plt.imshow的机会可能会比较多，读一下它的文档，百度一些资料，多了解些关于imshow的功能。












