[返回上一级](..)

[上一课](../03-基础语法)

# 使用PyCharm建立一个最基础的项目Hello World

前面，我们已经完成了对python基础语法的讲解，接下来，进入以实践为主的部分，配合编程实例逐一介绍python中的各种数据结构和运算。

本节中，我们使用PyCharm建立、运行一个最基础的项目，步骤如下：
1. 打开PyCharm；
2. 点击New Project新建一个项目；![图片加载错误](../imgs/0401.PNG "新建一个项目")
3. 配置项目的保存路径和main脚本；![图片加载错误](../imgs/0402.PNG "配置项目")
4. 点击Create创建项目；![图片加载错误](../imgs/0403.PNG "创建项目")
5. 此时，由于我们第三步的配置，项目中默认会有一个main.py文件，并包含一些代码，在这里，我们将所有的代码删除，填写如下代码，完成Hello World项目：
```python
print('Hello','World')
```
6. 点击右上角的“绿色三角按钮”，运行此项目中的main.py脚本，运行结果如下图。图中的main.py文件只有一条语句，输出结果在PyCharm的下方。![图片加载错误](../imgs/0404.PNG "运行结果")

## 使用console运行命令

PyCharm的功能有很多，全部一一介绍，是互相折磨，这里捎带着介绍一个，除了上述建立项目，运行脚本的方式以外，在PyCharm中，还有另外一种方式运行python命令，点击左下角的“Python Console”可以启动python的一个端，可以在里面逐行的执行python语句，同时看到内存的变化情况：
![图片加载错误](../imgs/0405.PNG "使用console")
![图片加载错误](../imgs/0406.PNG "使用console")

[下一课](../05-数字)

## 课后习题

使用PyCharm建立自己的hello world项目。

不适用PyCharm实现hello.py脚本，使用python命令，执行hello.py。





















