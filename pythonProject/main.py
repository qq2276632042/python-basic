import numpy as np
import gzip
import matplotlib.pyplot as plt

paths = [
	'train-labels-idx1-ubyte.gz', 'train-images-idx3-ubyte.gz',
	't10k-labels-idx1-ubyte.gz', 't10k-images-idx3-ubyte.gz'
]
with gzip.open(paths[0], 'rb') as lbpath:
	y_train = np.frombuffer(lbpath.read(), np.uint8, offset=8)

with gzip.open(paths[1], 'rb') as imgpath:
	x_train = np.frombuffer(
	imgpath.read(), np.uint8, offset=16).reshape(len(y_train), 28, 28)

with gzip.open(paths[2], 'rb') as lbpath:
	y_test = np.frombuffer(lbpath.read(), np.uint8, offset=8)

with gzip.open(paths[3], 'rb') as imgpath:
	x_test = np.frombuffer(imgpath.read(), np.uint8, offset=16).reshape(len(y_test), 28, 28)

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

plt.figure(figsize=(10,10))
for i in range(25):
	plt.subplot(5,5,i+1)
	plt.xticks([])
	plt.yticks([])
	plt.grid(False)
	plt.imshow(x_train[i], cmap=plt.cm.binary)
	plt.xlabel(class_names[y_train[i]])
plt.show()