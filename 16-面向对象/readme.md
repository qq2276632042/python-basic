[返回上一级](..)

[上一课](../15-输入和输出)

# 面向对象

python语言是一种面向对象的语言，类的定义和使用都比较简单，未来的这段时间，大家读别人的代码，遇到类的定义和使用的机会应该是比较多的。类的定义和使用的规则如下：

```python
#定义一个类
class People:
	#私有变量的名称前有两个下划线，共有成员没有，函数也一样，以下划线来区分是否是私有的。
	name='null'
	is_man=None
	__age=-1
	__weight=-1
	#每一个成员函数都需要一个self参数，self同java中的this指针，代表的是调用这个函数的对象的本身。
	##构造函数，self是隐性传递的参数，调用这些函数的时候，不必考虑self参数。
	def __init__(self,x):
		self.name=x
people1=People('张三')
people1.is_man=True
people2=People('李四')
people2.is_man=False
```

```python
class Student(People):
	__password='null'
	id=-1
	#重写构造函数，要求，必须有姓名和id，才可以构造
	def __init__(self,x,y):
		People.__init__(self,x)
		self.id=y
	def set_password(self,x):
		self.__password=x
	def is_right_password(self,x):
		return self.__password==x
#使用Student类
student1=Student('张三',1)
student2=Student('李四',2)
student1.set_password('123')
student2.set_password('456')
if student1.is_right_password('123'):
	print('密码正确')
else:
	print('密码错误')
```

## 课后习题

- 完善Student类，构建一个列表，存储一堆Student对象，根据年龄排序，从小到大，打印。

[下一课](../17-numpy)























