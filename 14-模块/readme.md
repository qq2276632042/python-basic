[返回上一级](..)

[上一课](../13-函数)

# 模块

模块是一个包含所有你定义的函数和变量的文件，其后缀名是.py。模块可以被别的程序引入，以使用该模块中的函数等功能。这也是使用 python 标准库的方法。

针对本次的集训课程，大家会用到自己定义模块的时候，基本没有，基本都是在使用别人的模块。

## 定义模块，并使用自定义模块

1. 在当前项目的目录里，建立一个文件夹（包）：pack_a；
  ![加载错误](../imgs/1403.PNG)

2. 在pack_a这个文件夹下，建立一个空文件：\_\_init\_\_.py，这个文件的名称可以告诉python，这个文件夹，是一个包；

   ![加载错误](../imgs/1404.PNG)

3. 在pack_a文件夹下建立一个脚本文件funcs.py（模块），在这个文件里，你可以实现各种函数；

4. 至此，最简单的包pack_a构建完成，该包只包含一个模块funcs，该模块中包含若干函数；

5. 进一步，可以在pack_a文件夹（包）下建立一个子文件夹（子包）pack_b，子包也同样需要包含\_\_init\_\_.py，以及若干个具体的模块(.py)文件；

   ![加载错误](../imgs/1405.PNG)

6. 具体的实现，参考本网页根目录下的pythonProject文件夹，如何使用这个包以及其中的模块，参考下面的代码：

```python
#导入模块pack_a.funcs
import pack_a.funcs
print(pack_a.funcs.add(2,4))
#从包pack_a导入模块funcs
from pack_a import funcs
print(funcs.sub(2,4))
#从模块pack_a.funcs导入函数add
from pack_a.funcs import add
print(add(2,4))
#导入pack_a中所有的模块
from pack_a import *
print(funcs.add(2,4))
#导入模块并重命名
from pack_a import funcs as a
from pack_a.pack_b import funcss as b
print(a.add(2,4),a.sub(2,4),b.mul(2,4),b.div(2,4))
```

## 查看、使用已经安装好的第三方包

### 查看已安装包

打开cmd命令行窗口，输入pip list，即可查看已经安装的所有python第三方包，及这些包的版本信息。

### 使用第三方包

对于已经安装好的别人的包，例如numpy包：

```python
import numpy as np
a=np.ones((2,4),dtype=np.float)
print(a)
```

## 课后习题

- 本次集训中，大家只要知道，如何使用别人提供的第三方包，即可，是没有机会自己去写包的，有兴趣的同学，可以自己做些实验，像上面提到的一样，尝试自定义自己的包和模块。

[下一课](../15-输入和输出)























