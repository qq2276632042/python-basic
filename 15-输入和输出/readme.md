[返回上一级](..)

[上一课](../14-模块)

# 输入，输出

输入输出，主要可以分为，针对控制台的输入输出，和针对文件的输入输出。

python很少用来写控制台输入的程序，所以我不打算介绍这部分知识，针对控制台的输出，大多是用print，用起来也很简单，所以这个部分，我们主要学习以下，针对文件的输入输出，后继内容中，大家很可能需要用到。

## 读文件

```python
#针对小文件，可以一次读进内存
content=open('mips.txt').readlines()
print(content[:4])
#content是一个列表，列表的每一个元素对应着文件中的每一行，通过对这个列表做进一步的处理来处理文件。
#针对大文件，可能无法一次读进内存
f=open('mips.txt')
s=f.readline()
#f.readline()每次读取的是文件的一行内容，到文件末尾时，这个函数返回空字符串。
while s!='':
	print(len(s.split('\t')))
	s=f.readline()
f.close()
```

## 写文件

```python
#批量写
content=open('mips.txt').readlines()
content=filter(lambda x:len(x.split('\t'))<=4,content)
open('tmp.txt','w').writelines(list(content))
#一点一点写
f=open('mips.txt')
fo=open('tmp.txt','w')
s=f.readline()
while s!='':
	if len(s.split('\t'))<=4:
		fo.write(s)
	s=f.readline()
f.close()
fo.close()
```

## 课后习题

- 整理出mips.txt中所有的关键字（去重），存储在另一个文件中。

[下一课](../16-面向对象)























