[返回上一级](..)

[上一课](../11-分支语句)

# 循环语句

## for循环

```python
import time
for i in range(100):
	time.sleep(0.1)
	print('\r%.2f%%'%(i/99*100),end='')
a=list(range(100))
b={}
for i in a:
	b['key'+str(i)]='value%d'%i
for k in b:
	print('%s=>%s'%(k,b[k]))
```

## while循环

```python
import time
i=0
while i<100:
	time.sleep(0.1)
	print('\r%.2f%%'%(i/99*100),end='')
	i+=1
a=list(range(100))
b={}
i=0
while i<len(a):
	b['key'+str(i)]='value%d'%i
	i+=1
keys=list(b.keys())
i=0
while i<len(keys):
	print('%s=>%s'%(keys[i],b[keys[i]]))
	i+=1
```

## continue和break

- continue，结束当前循环，进入下一次循环；
- break，结束当前循环，跳出循环；

```python
for i in range(100):
	if i==13*4:
		break
	print(i,end=',')
print()
for i in range(100):
	if i%7==0 or '7' in str(i):
		continue
	print(i,end=',')
print()
i=0
while i<100:
	if i%7==0 or '7' in str(i):
		i+=1
		continue
	print(i,end=',')
	i+=1
print()
```

## 课后习题

- 从1打印到100，对于7的倍数，或包含7的数字，以\*替代。
- 计算字典中，值的和。
- 对于一个字符串，只保留其中的字母，去掉数字以及其他特殊字符。

[下一课](../13-函数)























