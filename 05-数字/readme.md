[返回上一级](..)

[上一课](../04-hello-world)

# 数字(Number)

## 数字类型

python中数字有四种类型：整数、布尔型、浮点数和复数。

1. int (整数), 如 1, 只有一种整数类型 int，并没有其他语言中的byte，short，long，此外，python中的int类型理论上无上限。
2. bool (布尔), 如 True、False。
3. float (浮点数), 如 1.23、3E-2
4. complex (复数), 如 1 + 2j、 1.1 + 2.2j

```python
#int类型演示，计算100的阶乘，int型理论无上限
a=1
for i in range(1,100+1):
	a=a*i
print(a)
#bool类型演示
b=False
if a%91==0:
	b=True
print("a可以整除91：",b)
b=False
if a%101==0:
	b=True
print("a可以整除101：",b)
#float类型演示，精度有限
a=1
i=0.01
while i<1:
	a=a*i
	i+=0.01
i=0.01
while i<1:
	a=a*4
	i+=0.01
print(a)
#complex类型演示
a=2 + 2j
b=2 + 4j
c= a+b
print(type(a),a,a.real,a.imag)
print(type(b),b,b.real,b.imag)
print(type(c),c,c.real,c.imag)
```

## python中数字类型的转换

有时候，我们需要对数据内置的类型进行转换，一般来说，只需要将数据类型作为函数名，调用即可，这些函数会根据参数的类型，自动调用对应的方法，返回一个满足要求的值：

```python
x_str='123456.654321'
x_float=float(x_str)
x_int=int(x_float)
#下面一条语句会报错，这是因为，x_str无法转化成一个int整数。
#x_int=int(x_str)
x_complex=complex(x_str)
x_complex=complex(x_int,x_float)
x_bool=bool(x_int)
```

## python数学运算

针对数字类型，python包含非常多的数字运算符号和函数，最常用的便是+，-，*，/，%，=，\*\*：

```python
a=2
b=a+2
print(a+b)
print(a-b)
print(a*b)
print(a/b)
print(b**2)
print(a*365%(b-1))
#//符号，不常用，除法，仅保留整数部分
print(a*365/(b-1))
print(a*365//(b-1))
```

下面，简单的罗列一些python的函数，根据经验，大多数都是常用的，其中的一些函数，如果无法直接调用，一般会在math包中。

```python
import math
a=123456.654321
print(abs(a))
print(math.ceil(a))
print(math.exp(a/123456))
print(math.fabs(a))
print(math.floor(a))
print(math.log(a))
print(math.log10(a))
print(math.modf(a))
print(round(a))
print(math.sqrt(a))
print(math.sin(a))
print(math.pi)
print(math.e)
```

### 数学函数

|函数|返回值( 描述 )|
| - | - |
|abs(x)|返回数字的绝对值，如abs(-10) 返回 10|
|ceil(x)|返回数字的上入整数，如math.ceil(4.1) 返回 5|
|exp(x)|返回e的x次幂(ex),如math.exp(1) 返回2.718281828459045|
|fabs(x)|返回数字的绝对值，如math.fabs(-10) 返回10.0|
|floor(x)|返回数字的下舍整数，如math.floor(4.9)返回 4|
|log(x)|如math.log(math.e)返回1.0,math.log(100,10)返回2.0|
|log10(x)|返回以10为基数的x的对数，如math.log10(100)返回 2.0|
|max(x1, x2,...)|返回给定参数的最大值，参数可以为序列。|
|min(x1, x2,...)|返回给定参数的最小值，参数可以为序列。|
|modf(x)|返回x的整数部分与小数部分，两部分的数值符号与x相同，整数部分以浮点型表示。|
|pow(x, y)|x\*\*y 运算后的值。|
|round(x [,n])|返回浮点数 x 的四舍五入值，如给出 n 值，则代表舍入到小数点后的位数。|
|sqrt(x)|返回数字x的平方根。|

### 随机数函数
|函数|描述|
| - | - |
|choice(seq)|从序列的元素中随机挑选一个元素，比如random.choice(range(10))，从0到9中随机挑选一个整数。|
|randrange ([start,] stop [,step])|从指定范围内，按指定基数递增的集合中获取一个随机数，基数默认值为 1|
|random()|随机生成下一个实数，它在[0,1)范围内。|
|seed([x])|改变随机数生成器的种子seed。如果你不了解其原理，你不必特别去设定seed，Python会帮你选择seed。|
|shuffle(lst)|将序列的所有元素随机排序|
|uniform(x, y)|随机生成下一个实数，它在[x,y]范围内。|

### 三角函数
|函数|描述|
| - | - |
|acos(x)|返回x的反余弦弧度值。|
|asin(x)|返回x的反正弦弧度值。|
|atan(x)|返回x的反正切弧度值。|
|atan2(y, x)|返回给定的 X 及 Y 坐标值的反正切值。|
|cos(x)|返回x的弧度的余弦值。|
|hypot(x, y)|返回欧几里德范数 sqrt(x*x + y*y)。|
|sin(x)|返回的x弧度的正弦值。|
|tan(x)|返回x弧度的正切值。|
|degrees(x)|将弧度转换为角度,如degrees(math.pi/2) ， 返回90.0|
|radians(x)|将角度转换为弧度|

### 数学常量
|常量|描述|
| - | - |
|pi|数学常量 pi（圆周率，一般以π来表示）|
|e|数学常量 e，e即自然常数（自然常数）。|

## 课后习题

- python 数字求和
- python 平方根
- python 二次方程
- python 计算三角形的面积
- python 计算圆的面积

```python
#举例，计算园的面积
import math
r=4
s=math.pi*(r**2)
print('半径为%f的园的面积使%f'%(r,s))
```

[下一课](../06-字符串)























